ferroica
--------

A set of tools for simulation and visualization of first-order
reversal curve (FORC) diagrams of single-domain (SD) particles.
Solutions are obtained via gradient-descent energy minimisation.
USAGE EXAMPLE:
  >>> import ferroica
  >>> lab = ferroica.forc_lab()
  >>> lab.set_saturation(field= 50.0, unit= 'mT')
  >>> lab.set_field_step(step=   0.5, unit= 'mT')
  >>> lab.set_field((0.3, 0.2, 0.8))
  >>> lab.set_material('magnetite')
  >>> forc_data = lab.run()
  >>> lab.plot_curves(forc_data)
  >>> rho = lab.forc_distribution(forc_data, sf= 1)
  >>> lab.heatmap(rho)
  >>> np.save('./forc_data.npy', forc_data)

ferroscript
-----------

For quick access to ferroica.forc_lab, a command-line tool for setting
up simulations and visualizations should be available to the user's PATH
on a succesful installation.
USAGE EXAMPLE:
  $ ferroscript --saturation 50 --step 0.5 --unit mT --field 0.3 0.2 0.8 --material magnetite --save forc_data
