#
# Copyright (C) 2018 M. A. Valdez-Grijalva
#
# This file is part of ferroica.
#
# ferroica is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ferroica is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import numpy  as np
import random as rm
import matplotlib.pyplot as plt
import scipy.linalg
import matplotlib.cm
from matplotlib.colors import LinearSegmentedColormap

class forc_lab:
    'A set of tools for simulation and visualization of FORC diagrams.'
    def __init__(self, saturation= 50.0,
                       step=        1.0,
                       unit=     'mT',
                       material= 'magnetite',
                       ensemble= 'random',
                       ncells=  50,
                       index = 234,
                       tau = 0.5,
                       c =   0.5,
                       gamma = 10.,
                       etol= 1.e-12,
                       gtol= 1.e-12,):
        # The Armijo-Goldstein parameters.
        self.set_minimizer_params(tau=   tau,
                                  c=     c,
                                  gamma= gamma,
                                  etol=  etol,
                                  gtol=  gtol,
        )

        # The acquisition parameters.
        self.set_saturation(field= saturation, unit= unit)
        self.set_field_step(step= step, unit= unit)

        # The material.
        self.set_material(material)

        # The field orientation.
        self.set_ensemble(kind= ensemble, n= ncells)
        self.set_index(index= index)


    ############################################################################
    #                   ACCESORS                                               #
    ############################################################################
    def set_saturation(self, field= 50., unit= 'mT'):
        '''
        Set the saturation field to be used.
        Specify value and units ('T', 'mT', 'uT', 'nT', 'kA/m',
        'A/m', 'mA/m', 'uA/m', 'kOe', 'Oe', 'mOe').
        '''
        self.user_supplied_saturation = field
        self.saturation = field * self.conversions(unit)
        self.unit = unit


    def set_field_step(self, step= 1., unit= 'mT'):
        '''
        Set the field step to be used.
        Specify value and units ('T', 'mT', 'uT', 'nT', 'kA/m',
        'A/m', 'mA/m', 'uA/m', 'kOe', 'Oe', 'mOe').
        '''
        self.user_supplied_step = step
        self.step = step * self.conversions(unit)
        self.n = 1+int(round((2.*self.saturation)/self.step))


    def set_minimizer_params(self, tau= 0.5,
                                   c=   0.5,
                                   gamma= 10.,
                                   etol= 1.e-12,
                                   gtol= 1.e-12,):
        '''
        Set the minimizer parameters. These have default values.
        Do not change unless you know what you're doing.
        '''
        self.tau = tau
        self.c   = c
        self.gamma = gamma
        self.etol = etol
        self.gtol = gtol
        self.gdparams = {'tau'  : self.tau,
                         'c'    : self.c,
                         'gamma': self.gamma,
                         'etol' : self.etol,
                         'gtol' : self.gtol}


    def set_material(self, material= 'magnetite'):
        '''
        Set the material. Either a string ('magnetite' or 'iron'),
        or a dictionary with the keys/values {'Ms': _, 'K1': _, 'K2': _,}.
        '''
        if type(material) == str:
            self.Ms = self.materials(material)['Ms']
            self.K1 = self.materials(material)['K1']
            self.K2 = self.materials(material)['K2']
            self.matparams = self.materials(material)
            self.custom_material = False
        elif type(material) == dict:
            self.Ms = material['Ms']
            self.K1 = material['K1']
            self.K2 = material['K2']
            self.matparams = material
            self.custom_material = True
        self.material = material


    def set_ensemble(self, kind= 'grid', n= 50):
        '''
        Set the ensemble ('grid' or 'random').
        For 'grid' ensembles, the sphere sector bounded by
        0 <= (theta, phi) <= pi/2 is divided into 'n*n' cells
        and particle/field orientations are taken from these.
        For 'random' ensembles, particle/field orientations
        are taken from a random distribution uniform over the sphere.
        '''
        self.ensemble_kind = kind
        self.ncells = n


    def set_index(self, index= 0):
        '''
        Set the index of the simulation.
        Sets the applied field for ensemble-type simulations.
        '''
        self.index = index
        if self.ensemble_kind == 'grid':
            n = self.ncells
            phi, theta = np.meshgrid(np.linspace(0., np.pi/2.-(np.pi/2.)/n, n),
                                     np.linspace(0., np.pi/2.-(np.pi/2.)/n, n),
            )
            phi   = np.ravel(phi)
            theta = np.ravel(theta)

            phi   += ((np.pi/2.)/n)/2.
            theta += ((np.pi/2.)/n)/2.

            x      = np.sin(theta[self.index])*np.cos(phi[self.index])
            y      = np.sin(theta[self.index])*np.sin(phi[self.index])
            z      = np.cos(theta[self.index])
        elif self.ensemble_kind == 'random':
            rm.seed(self.index)

            u     = rm.uniform(0.0, 0.25)
            v     = rm.uniform(0.5, 1.00)

            phi   = 2.*np.pi*u
            theta = np.arccos(2.*v - 1.)

            x     = np.sin(theta)*np.cos(phi)
            y     = np.sin(theta)*np.sin(phi)
            z     = np.cos(theta)

        self.field = np.array((x, y, z))
        self.user_supplied_field = False
        return self.field


    def set_field(self, field):
        '''
        Set the applied field orientation.
        '''
        field  = np.array(field)
        field /= np.sqrt(np.dot(field, field))
        self.field = field
        self.set_ensemble('none')
        self.user_supplied_field = True
        return self.field


    ############################################################################
    #                   SOLUTION LOOPS                                         #
    ############################################################################
    def hloop(self):
        '''
        Perform the hysteresis half-loop. Returns the reduced mag. and
        the (theta, phi)-angles.
        '''

        # Initial guess.
        if self.ensemble_kind != 'none': rm.seed(self.index)
        u = rm.uniform(0., 1.)
        v = rm.uniform(0., 1.)
        phi = 2.*np.pi*u
        theta = np.arccos(2.*v - 1.)

        # Applied field setup.
        applied = np.linspace(self.saturation, -self.saturation, self.n)

        # Solution loop.
        mag = np.zeros(self.n)
        path = np.zeros((self.n, 2))
        for i, B in enumerate(applied):
            theta, phi, _ = self.minimize(theta= theta, phi= phi, B= B)
            mag[i]  = np.dot(self.to_cartesian(theta, phi), self.field)
            path[i] = np.array((theta, phi))

        return mag, path


    def reversal_curves(self, main_branch, path):
        '''
        Perform the necessary reversal curves.
        '''
        discontinuities = self.discontinuities(path[::,0], path[::,1])
        N_discontinuities = len(discontinuities)

        # Solution loop.
        mFORCs = []
        for Ba, index in discontinuities:
            N_thisFORC = 1 + int(round((self.saturation-Ba)/self.step))
            magF = np.zeros(N_thisFORC)
            theta, phi = path[int(round(index))]

            magF[0] = np.dot(self.to_cartesian(theta, phi), self.field)
            applied_thisFORC = np.linspace(Ba+self.step,
                                           self.saturation,
                                           N_thisFORC-1)
            for i, B in enumerate(applied_thisFORC):
                theta, phi, _ = self.minimize(theta= theta, phi= phi, B= B)
                magF[i+1]  = np.dot(self.to_cartesian(theta, phi), self.field)

            mFORCs.append(np.array(magF))

        mFORCs = np.array(mFORCs)

        forc_data = []
        forc_data.append(main_branch)
        for f in mFORCs: forc_data.append(f)
        forc_data = np.array(forc_data)
        return forc_data


    def run(self):
        '''
        Perform the FORC simulation.
        '''
        #self.print_simulation_metadata()

        main_branch, path = self.hloop()
        forc_data = self.reversal_curves(main_branch, path)
        return forc_data


    ############################################################################
    #                   SOLVERS                                                #
    ############################################################################
    def energy(self, theta= 0., phi= 0., B= 0.):
        '''
        Calculate the energy.
        '''
        xi, psi, omega = self.field
        Ms = self.Ms
        K1 = self.K1
        K2 = self.K2

        return ((K1/Ms)*np.power(np.sin(theta), 2)*(
                  np.power(np.cos(theta), 2) +
                    np.power(np.sin(theta)*np.cos(phi)*np.sin(phi), 2)) +
                (K2/Ms)*np.power(np.power(np.sin(theta), 2)*
                  np.cos(theta)*np.sin(phi)*np.cos(phi), 2) -
                B*(xi*np.sin(theta)*np.cos(phi) +
                   psi*np.sin(theta)*np.sin(phi) +
                   omega*np.cos(theta)))


    def grad(self, theta= 0., phi= 0., B= 0.):
        '''
        Calculate the gradient.
        '''
        xi, psi, omega = self.field
        Ms = self.Ms
        K1 = self.K1
        K2 = self.K2
        ehat_theta = (2.*(K1/Ms)*np.sin(theta)*np.cos(theta)*(
                        2.*np.power(np.sin(theta)*np.sin(phi)*np.cos(phi), 2) -
                      np.power(np.sin(theta), 2)+np.power(np.cos(theta), 2)) +
                      2.*(K2/Ms)*np.sin(theta)*np.cos(theta)*(
                        np.power(np.sin(phi)*np.cos(phi), 2)*(
                        2.*np.power(np.sin(theta)*np.cos(theta), 2) -
                          np.power(np.sin(theta), 4))) -
                      B*(xi*np.cos(theta)*np.cos(phi) +
                        psi*np.cos(theta)*np.sin(phi)-omega*np.sin(theta))
                      )
        ehat_phi = (2.*(K1/Ms)*np.power(np.sin(theta), 4)*
                      np.sin(phi)*np.cos(phi)*(
                        np.power(np.cos(phi), 2)-np.power(np.sin(phi), 2)) +
                    2.*(K2/Ms)*np.power(np.sin(theta), 4)*
                      np.sin(phi)*np.cos(phi)*(
                        np.power(np.cos(theta), 2)*(np.power(np.cos(phi), 2) -
                                                    np.power(np.sin(phi), 2))) -
                    B*np.sin(theta)*(psi*np.cos(phi)-xi*np.sin(phi))
                    )

        return np.array((ehat_theta, ehat_phi))


    def linesearch(self, theta= 0., phi= 0., B= 0.):
        '''
        The line-search. Calculate an appropriate step for the gradient descent.
        '''
        Ms = self.Ms
        K1 = self.K1
        K2 = self.K2
        tau = self.tau
        c   = self.c

        g = self.grad(theta= theta, phi= phi, B= B)

        gamma = self.gamma
        while (self.energy(theta= theta-gamma*g[0], phi= phi-gamma*g[1], B= B) >
               self.energy(theta= theta, phi= phi, B= B) - c*gamma*np.dot(g, g)):
            gamma *= tau

        return gamma


    def minimize(self, theta= 0., phi= 0., B= 0.):
        '''
        The gradient descent minimizer.
        '''
        g =     self.grad(theta= theta, phi= phi, B= B)
        nrg = self.energy(theta= theta, phi= phi, B= B)

        etol = self.etol
        gtol = self.gtol

        # Solution loop.
        nrg_old = 1.e12 # dummy value to get the while loop started.
        path = []
        path.append(np.array((theta, phi)))
        while (np.dot(g, g) > gtol or self.Ms*abs(nrg_old-nrg) > etol):
            gamma = self.linesearch(theta= theta, phi= phi, B= B)
            theta_old, phi_old = theta, phi
            theta += -gamma*g[0]
            phi   += -gamma*g[1]

            nrg_old = self.energy(theta= theta_old, phi= phi_old, B= B)
            nrg     = self.energy(theta= theta,     phi= phi,     B= B)
            g       =   self.grad(theta= theta,     phi= phi,     B= B)

            path.append(np.array((theta, phi)))

        return theta, phi, np.array(path)


    ############################################################################
    #                   RHO CALCULATION                                        #
    ############################################################################
    def forc_distribution(self, forc_data, sf= 1):
        '''
        Calculate the FORC distribution.
        '''
        #forc_data = self.forc_data
        mFORC = self.m_forc(forc_data)
        mFORC *= self.Ms

        rho = np.zeros_like(mFORC)
        B = np.linspace(-self.saturation, self.saturation, self.n)
        Bb, Ba = np.meshgrid(B, -B)
        for i in range(self.n):
            for j in range(self.n-1-i, self.n):
                grid = self.in_grid(i, j, sf= sf)
                data = []
                for indices in grid:
                    data.append(np.array((Bb[indices[0], indices[1]],
                                          Ba[indices[0], indices[1]],
                                          mFORC[indices[0], indices[1]])))

                data = np.array(data)
                A = np.c_[np.ones(data.shape[0]), data[:,:2],
                          np.prod(data[:,:2], axis=1),
                          np.power(data[:,:2], 2)]
                C,_,_,_ = scipy.linalg.lstsq(A, data[:,2])
                rho[i, j] = -(C[3]/2.)

        return rho


    def in_grid(self, i, j, sf= 1):
        '''
        Is the point (i, j) in the FORC distribution calc. local grid?
        '''
        grid = []
        for k in range(i-sf, i+sf+1):
            for l in range(j-sf, j+sf+1):
                if self.in_domain(k, l):
                    grid.append((k, l))

        return tuple(grid)


    def in_domain(self, i, j):
        '''
        Is the point (i, j) in the FORC domain?
        '''
        n = self.n
        in_square = (i < n) and (j < n)
        in_triangle = (j+1 >= n-i)
        return True if (in_square and in_triangle) else False


    def m_forc(self, forc_data):
        '''
        Produce the m=m(Bb, Ba) function from the forc_data object.
        '''

        # the main branch
        main_branch = forc_data[0]

        # the mFORC array
        m = np.zeros((self.n, self.n))

        # main branch to diagonal
        for i in range(self.n):
            m[i, self.n-1-i] = main_branch[i]

        # forcs to rows
        for forc in forc_data[1:]:
            m[len(forc)-1, len(main_branch)-len(forc):][:] = forc[:]

        # fill array
        row_indices = np.zeros(len(forc_data[1:]), dtype=int)
        for i, forc in enumerate(forc_data[1:]):
            row_indices[i] = len(forc)

        start = 1
        for index in row_indices:
            for i in range(start, index-1):
                for j in range(self.n-i, self.n):
                    m[i, j] = m[i-1, j]
            start = index

        for i in range(start, self.n):
            for j in range(self.n-i, self.n):
                m[i, j] = m[i-1, j]

        return m


    ############################################################################
    #                   PLOTTING                                               #
    ############################################################################
    def plot_curves(self, forc_data, label= '', mode= 'show', block= False):
        '''
        Plot the hysteresis curves.
        '''
        n = 1+int(round((2.*self.saturation)/self.step))
        applied = np.linspace(self.saturation, -self.saturation, n)

        fig, ax = plt.subplots()
        plt.ioff()
        plt.plot(applied, forc_data[0], color= 'k')
        for forc in forc_data[1:]:
            plt.plot(applied[:len(forc)][::-1], forc, color= 'k')
            plt.scatter(applied[len(forc)-1], forc[0], marker= 'o', color= 'k')

        plt.xlabel(r'$B\,(\mathrm{T})$')
        plt.ylabel(r'$m$', rotation= 'horizontal')

        plt.title(r'$\hat{n}=(%.4f, %.4f, %.4f)$' % tuple(self.field))

        plt.draw()
        if mode == 'save':
            plt.savefig('./CURVES%s.pdf' % label)
        elif mode == 'show':
            plt.show(block= block)


    def contour(self, rho, color= 'rb', mode= 'show',
                           label= '', lines= True, linear= False, block= False):
        '''
        Plot the forc distribution.
        '''
        rho = rho.copy()
        rhomax_true = rho.max()
        rhomin_true = rho.min()
        rho *= (1./rhomax_true)

        rhomin_false = rho.min() # normalise negative values
        for i in range(len(rho)):
            for j in range(len(rho[0])):
                if rho[i, j] < 0.:
                    rho[i, j] *= (-1./rhomin_false)

        color_scheme = {'rb': matplotlib.cm.RdBu_r, 'bw': matplotlib.cm.Greys}
        color_scheme = color_scheme[color]
        shiftedCMap = self.shiftedColorMap(color_scheme, midpoint= 0.5)

        if linear:
            levels = np.linspace(-1., 1., 21)
        elif not linear:
            levels = np.array([2.**-7, 2.**-6, 2.**-5, 2.**-4, 2.**-3, 2.**-2,
                               0.5,    0.6,    0.7,    0.8,    1.0])
            levels = np.concatenate((-1.0*levels[::-1], levels))

        sat = self.saturation
        Bx, By = np.meshgrid(np.linspace(-sat,  sat, self.n),
                             np.linspace( sat, -sat, self.n))

        fig, ax = plt.subplots()
        plt.ioff()
        figure = plt.contourf(Bx, By, rho,
                              cmap= shiftedCMap, levels= levels)

        plt.plot([-sat, sat], [-sat, sat], color= 'black', ls= '-', lw= 0.5)
        plt.plot([0., sat], [0., -sat], color= 'black', ls= '--', lw= 0.25)
        plt.xlabel(r'$B_b\, (\mathrm{mT})$')
        plt.ylabel(r'$B_a\, (\mathrm{mT})$', rotation= 'vertical')

        clb = plt.colorbar(figure)
        clb.ax.set_title(r'$ \rho $', y= 1.01)
        if lines:
            contours = plt.contour(Bx, By, rho,
                                   levels= levels, colors= 'k',
                                   linewidths= 0.1, linestyles= '--')
            clb.add_lines(contours)

        if linear:
            ticks_up   = [r'$ 0.1$', r'$ 0.2$',
                          r'$ 0.3$', r'$ 0.4$',
                          r'$ 0.5$', r'$ 0.6$',
                          r'$ 0.7$', r'$ 0.8$',
                          r'$ 0.9$', r'$ 1.0$']
            ticks_down = [r'$ 0.0$', r'$-0.1$',
                          r'$-0.2$', r'$-0.3$',
                          r'$-0.4$', r'$-0.5$',
                          r'$-0.6$', r'$-0.7$',
                          r'$-0.8$', r'$-0.9$',
                          r'$-1.0(%.2f)$' % (abs(rhomin_true)/rhomax_true)]
            ticks_down.reverse()
        elif not linear:
            ticks_up   = [r'$2^{-7}$',  r'$2^{-6}$',
                          r'$2^{-5}$',  r'$2^{-4}$',
                          r'$2^{-3}$',  r'$2^{-2}$',
                          r'$0.5$',       r'$0.6$',
                          r'$0.7$',       r'$0.8$',
                          r'$1.0$']
            ticks_down = [r'$-2^{-7}$', r'$-2^{-6}$',
                          r'$-2^{-5}$', r'$-2^{-4}$',
                          r'$-2^{-3}$', r'$-2^{-2}$',
                          r'$-0.5$',      r'$-0.6$',
                          r'$-0.7$',      r'$-0.8$',
                          r'$-1.0(%.2f)$' % (abs(rhomin_true)/rhomax_true)]
            ticks_down.reverse()

        clb.set_ticks(levels)
        clb.ax.set_yticklabels(np.concatenate((ticks_down, ticks_up)))
        plt.title(r'$\hat{n}=(%.4f, %.4f, %.4f)$' % tuple(self.field))

        ax.set_aspect('equal')
        plt.tight_layout()

        plt.draw()
        if mode == 'save':
            plt.savefig('./FORC%s.pdf' % label)
        elif mode == 'show':
            plt.show(block= block)


    def heatmap(self, rho, color= 'rb', mode= 'show', label= '', block= False):
        '''
        Plot the forc distribution. Heat map.
        '''
        rho = rho.copy()
        rhomax_true = rho.max()
        rhomin_true = rho.min()
        rho *= (1./rhomax_true)

        rhomin_false = rho.min() # normalise negative values
        for i in range(len(rho)):
            for j in range(len(rho[0])):
                if rho[i, j] < 0.:
                    rho[i, j] *= (-1./rhomin_false)

        color_scheme = {'rb': matplotlib.cm.RdBu_r, 'bw': matplotlib.cm.Greys}
        color_scheme = color_scheme[color]
        shiftedCMap = self.shiftedColorMap(color_scheme, midpoint= 0.5)

        levels = np.linspace(-1., 1., 21)

        sat = self.saturation
        Bx, By = np.meshgrid(np.linspace(-sat,  sat, self.n),
                             np.linspace( sat, -sat, self.n))

        fig, ax = plt.subplots()
        plt.ioff()
        figure = plt.pcolor(Bx, By, rho, cmap= shiftedCMap)

        plt.plot([-sat, sat], [-sat, sat], color= 'black', ls= '-', lw= 0.5)
        plt.plot([0., sat], [0., -sat], color= 'black', ls= '--', lw= 0.25)
        plt.xlabel(r'$B_b\, (\mathrm{mT})$')
        plt.ylabel(r'$B_a\, (\mathrm{mT})$', rotation= 'vertical')

        clb = plt.colorbar(figure)
        clb.ax.set_title(r'$ \rho $', y= 1.01)

        ticks_up   = [r'$ 0.1$', r'$ 0.2$',
                      r'$ 0.3$', r'$ 0.4$',
                      r'$ 0.5$', r'$ 0.6$',
                      r'$ 0.7$', r'$ 0.8$',
                      r'$ 0.9$', r'$ 1.0$']
        ticks_down = [r'$ 0.0$', r'$-0.1$',
                      r'$-0.2$', r'$-0.3$',
                      r'$-0.4$', r'$-0.5$',
                      r'$-0.6$', r'$-0.7$',
                      r'$-0.8$', r'$-0.9$',
                      r'$-1.0(%.2f)$' % (abs(rhomin_true)/rhomax_true)]
        ticks_down.reverse()

        clb.set_ticks(levels)
        clb.ax.set_yticklabels(np.concatenate((ticks_down, ticks_up)))
        plt.title(r'$\hat{n}=(%.4f, %.4f, %.4f)$' % tuple(self.field))

        ax.set_aspect('equal')
        plt.tight_layout()

        plt.draw()
        if mode == 'save':
            plt.savefig('./FORC%s.pdf' % label)
        elif mode == 'show':
            plt.show(block= block)


    def shiftedColorMap(self, cmap, start=0, midpoint=0.5,
                              stop=1.0, name='shiftedcmap'):
        '''
        Returns a shifted colormap. Useful for divergent colormaps.
        Borrowed from stackoverflow user `Paul H`.
        '''
        cdict = {
            'red':   [],
            'green': [],
            'blue':  [],
            'alpha': [],
        }

        # regular index to compute the colors
        reg_index = np.linspace(start, stop, 257)

        # shifted index to match the data
        shift_index = np.hstack([
            np.linspace(0.0, midpoint, 128, endpoint= False), 
            np.linspace(midpoint, 1.0, 129, endpoint= True)
        ])

        for ri, si in zip(reg_index, shift_index):
            r, g, b, a = cmap(ri)

            cdict['red'].append((si, r, r))
            cdict['green'].append((si, g, g))
            cdict['blue'].append((si, b, b))
            cdict['alpha'].append((si, a, a))

        newcmap = LinearSegmentedColormap(name, cdict)
        plt.register_cmap(cmap= newcmap)

        return newcmap


    ############################################################################
    #                   UTILITIES                                              #
    ############################################################################
    def discontinuities(self, theta, phi):
        '''
        Identify the discontinuities along the hysteresis main branch.
        '''
        n = self.n
        applied = np.linspace(self.saturation, -self.saturation, n)

        mvectors = np.zeros((n, 3))
        for i in range(n): mvectors[i, :] = self.to_cartesian(theta[i], phi[i])

        forc_fields = [np.array((applied[i+1], i+1)) for i in range(n-1) if \
                       self.angle(mvectors[i], mvectors[i+1]) > 5.*np.pi/180.]
        forc_fields = np.array(forc_fields)
        return forc_fields


    def to_cartesian(self, theta, phi):
        '''
        Transform from spherical to cartesian coordinates (r=1).
        '''
        return np.array((np.sin(theta)*np.cos(phi),
                         np.sin(theta)*np.sin(phi),
                         np.cos(theta)))


    def angle(self, u, v):
        '''
        Calculate the angle between two unit vectors.
        '''
        u  = np.array(u)
        u /= np.sqrt(np.dot(u, u))

        v  = np.array(v)
        v /= np.sqrt(np.dot(v, v))
        return np.arccos(np.dot(u, v))


    def conversions(self, unit):
        '''
        Returns the conversion factor between `unit` and Tesla.
        '''
        mu0 = 4.*np.pi*1.e-7
        conversions = {'T'   : 1.,
                       'mT'  : 1.e-3,
                       'uT'  : 1.e-6,
                       'nT'  : 1.e-9,
                       'kA/m': mu0*1.e3,
                       'A/m' : mu0,
                       'mA/m': mu0*1.e-3,
                       'uA/m': mu0*1.e-6,
                       'kOe' : 1.e-1,
                       'Oe'  : 1.e-4,
                       'mOe' : 1.e-7,
        }
        return conversions[unit]


    def materials(self, material):
        '''
        Return the material parameters. SI MKS units.
        '''
        materials = {'magnetite': {'Ms':  4.8e5,
                                   'K1': -1.2e4,
                                   'K2':  0.   ,
                     },
                     'iron'     : {'Ms': 1.72e6,
                                   'K1': 4.80e4,
                                   'K2': 0.    ,
                     },
                     'greigite' : {'Ms':  2.27e5,
                                   'K1': -1.70e4,
                                   'K2':  0.    ,
                     },
        }
        return materials[material]


    def weight_factors(self, index):
        '''
        Return the weight factors for integrating the full response
        of `grid` ensembles: (sin(theta), d_theta, d_phi).
        '''
        n = self.ncells
        phi, theta = np.meshgrid(np.linspace(0., np.pi/2.-(np.pi/2.)/n, n),
                                 np.linspace(0., np.pi/2.-(np.pi/2.)/n, n),
        )
        phi   = np.ravel(phi)
        theta = np.ravel(theta)

        phi   += ((np.pi/2.)/n)/2.
        theta += ((np.pi/2.)/n)/2.

        d_phi   = (np.pi/2.)/n
        d_theta = (np.pi/2.)/n

        return (np.sin(theta[index]), d_theta, d_phi)


    def print_simulation_metadata(self):
        '''
        Print a card with the simulation meta-data.
        '''
        def pprint(*args):# aux. function
            key_length   = 14
            value_length = 36
            args_length  = sum([len(a) for a in args[1:]])
            row  = '| '
            row += args[0]
            row += ' '*(key_length-len(args[0])-2)
            for arg in args[1:]:
                row += '%s ' % arg
            row += ' '*(value_length - args_length - len(args))
            row += '|'
            print row

        saturation = self.user_supplied_saturation
        step       = self.user_supplied_step
        unit       = self.unit

        ensemble = self.ensemble_kind
        ncells   = self.ncells

        if not self.user_supplied_field:
            index = self.index

        material = self.material

        print '-'*50
        print '|', ' '*10, 'FORC simulation metadata', ' '*10, '|'
        print '-'*50

        pprint('saturation', '%9.2e' % saturation, unit)
        pprint('step',       '%9.2e' % step,       unit)
        
        if self.user_supplied_field:
            pprint('ensemble', ' none')
            pprint('ncells',   ' n/a' )
            pprint('index',    ' n/a' )
        else:
            pprint('ensemble', ' '+ensemble)
            if ensemble == 'random':
                pprint('ncells', ' n/a')
            elif ensemble == 'grid':
                pprint('ncells', ' '+str(ncells))
            pprint('index', ' '+str(index))

        pprint('field')
        pprint('  nx', ' %11.6e' % self.field[0])
        pprint('  ny', ' %11.6e' % self.field[1])
        pprint('  nz', ' %11.6e' % self.field[2])

        if self.custom_material:
            pprint('material', ' custom')
        else:
            pprint('material', ' '+material)

        pprint('  Ms', '%9.2e' % self.Ms, 'A/m'  )
        pprint('  K1', '%9.2e' % self.K1, 'J/m^3')
        pprint('  K2', '%9.2e' % self.K2, 'J/m^3')

        pprint('gdparams')
        pprint('  tau',   '%9.2e' % self.tau  )
        pprint('  c',     '%9.2e' % self.c    )
        pprint('  gamma', '%9.2e' % self.gamma)
        pprint('  etol',  '%9.2e' % self.etol )
        pprint('  gtol',  '%9.2e' % self.gtol )

        print '-'*50
