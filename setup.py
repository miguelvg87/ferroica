from setuptools import setup

def readme():
    with open('README.rst') as f:
        return f.read()

setup(name=         'ferroica',
      version=      '0.1',
      description=  'FORC simulation laboratory',
      url=          'https://gitlab.com/miguelvg87/ferroica',
      keywords=     'ferroica hysteresis forc magnetism',
      author=       'M. A. Valdez-Grijalva',
      author_email= 'miguel.valdesg@gmail.com',
      license=      'GPLv3',
      zip_safe=     False,
      packages=         ['ferroica'
      ],
      install_requires= ['numpy',
                         'scipy',
                         'matplotlib'
      ],
      scripts=          ['bin/ferroscript'
      ],
      classifiers=      [
        'Development Status :: 4 - Beta',
        'Programming Language :: Python :: 2.7',
        'Topic :: Scientific/Engineering :: Physics',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)'
      ],
      long_description= readme(),
      include_package_data= True,
)
